<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Question;

class QuestionImport implements ToModel, WithHeadingRow, WithValidation
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        Question::create([
            'package_id'        => request()->segment(2),
            'urut'              => $row['urut'],
            'jenis'             => $row['jenis'],
            'question'          => $row['question'],
            'option_a'          => $row['option_a'],
            'value_a'           => $row['value_a'],
            'option_b'          => $row['option_b'],
            'value_b'           => $row['value_b'],
            'option_c'          => $row['option_c'],
            'value_c'           => $row['value_c'],
            'option_d'          => $row['option_d'],
            'value_d'           => $row['value_d'],
            'option_e'          => $row['option_e'],
            'value_e'           => $row['value_e'],
            'answer'            => $row['answer'],
            'discussion'        => $row['discussion']
        ]);
    }

    public function rules(): array
    {
        return [
            '*.jenis' => ['required'],
        ];
    }
}
