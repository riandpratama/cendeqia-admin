<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Package;

class PackageImport implements ToModel, WithHeadingRow, WithValidation
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        Package::create([
            'name' => $row['nama'],
            'urut' => $row['urut']
        ]);
    }

    public function rules(): array
    {
        return [
            '*.nama' => ['required'],
            '*.urut' => ['required'],
        ];
    }
}
