<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TheoryDetail extends Model
{
    protected $table = 'theory_detail';

    protected $guarded = [];
}
