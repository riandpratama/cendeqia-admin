<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DescriptionQuiz extends Model
{
    protected $table = 'description_quiz';

    protected $guarded = [];
}
