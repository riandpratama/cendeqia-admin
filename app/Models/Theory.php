<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Theory extends Model
{
    protected $table = 'theory';

    protected $guarded = [];
}
