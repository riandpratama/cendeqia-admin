<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionUserDetail extends Model
{
   	protected $table = 'question_user_details';

   	protected $guarded = [];

   	public $timestamps = false;

   	public function package()
    {
    	return $this->belongsTo(Package::class, 'package_id')->withDefault();
    }
}
