<?php

namespace App\Http\Controllers;

use Alert;
use Session;
use Redirect;

use App\Models\Package;
use App\Models\Question;
use App\Imports\QuestionImport;
use App\Exports\QuestionsExport;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Package::with('question')->orderBy('urut', 'asc')->get();

        return view('backend.question.index', compact('data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $paket = Package::findOrFail($id);

        $data = Question::where('package_id', $id)->orderBy('urut', 'asc')->get();

        return view('backend.question.detail', compact('data', 'id', 'paket'));
    }

    public function storeExcel(Request $request, $id)
    {
        if ($request->hasFile('excel')) {
            $file = $request->file('excel');
            Excel::import(new QuestionImport, $file);
        }

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $paket = Package::findOrFail($id);

        return view('backend.question.create', compact('paket'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $paket = Package::findOrFail($id);

        $data = Question::where('package_id', $id)->where('urut', $request->urut)->first();

        if (isset($data->urut)) {
            Alert::error('Error!');
            return Redirect::route('question.detail', $id);
        }

        // if($request->urut === )

        if ($request->hasFile('img_question')) {
            $file = $request->file('img_question')->store('uploads');
        } else {
            $file = NULL;
        }

        if ($request->hasFile('img_option_a')) {
            $filename1 = $request->file('img_option_a')->store('uploads');
        } else {
            $filename1 = NULL;
        }

        if ($request->hasFile('img_option_b')) {
            $filename2 = $request->file('img_option_b')->store('uploads');
        } else {
            $filename2 = NULL;
        }

        if ($request->hasFile('img_option_c')) {
            $filename3 = $request->file('img_option_c')->store('uploads');
        } else {
            $filename3 = NULL;
        }

        if ($request->hasFile('img_option_d')) {
            $filename4 = $request->file('img_option_d')->store('uploads');
        } else {
            $filename4 = NULL;
        }

        if ($request->hasFile('img_option_e')) {
            $filename5 = $request->file('img_option_e')->store('uploads');
        } else {
            $filename5 = NULL;
        }

        if ($request->hasFile('img_discussion')) {
            $filenamedisucussion = $request->file('img_discussion')->store('uploads');
        } else {
            $filenamedisucussion = NULL;
        }

        Question::create([
            'urut' => $request->urut,
            'package_id' => $id,
            'jenis' => $request->jenis,
            'question' => $request->question,
            'option_a' => $request->option_a,
            'value_a' => $request->value_a,
            'option_b' => $request->option_b,
            'value_b' => $request->value_b,
            'option_c' => $request->option_c,
            'value_c' => $request->value_c,
            'option_d' => $request->option_d,
            'value_d' => $request->value_d,
            'option_e' => $request->option_e,
            'value_e' => $request->value_e,
            'answer' => $request->answer,
            'discussion' => $request->discussion,
            'img_question' => $file,
            'img_option_a' => $filename1,
            'img_option_b' => $filename2,
            'img_option_c' => $filename3,
            'img_option_d' => $filename4,
            'img_option_e' => $filename5,
            'img_discussion' => $filenamedisucussion,
        ]);

        Alert::success('Success!');

        // return redirect('question.detail',[$paket->id]);
        return Redirect::route('question.detail', $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Question::findOrFail($id);

        return view('backend.question.edit', compact('data', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Question::findOrFail($id);

        if ($request->hasFile('img_question')) {
            $file = $request->file('img_question')->store('uploads');
        } else {
            $file = $data->img_question;
        }

        if ($request->hasFile('img_option_a')) {
            $filename1 = $request->file('img_option_a')->store('uploads');
        } else {
            $filename1 = $data->img_option_a;
        }

        if ($request->hasFile('img_option_b')) {
            $filename2 = $request->file('img_option_b')->store('uploads');
        } else {
            $filename2 = $data->img_option_b;
        }

        if ($request->hasFile('img_option_c')) {
            $filename3 = $request->file('img_option_c')->store('uploads');
        } else {
            $filename3 = $data->img_option_c;
        }

        if ($request->hasFile('img_option_d')) {
            $filename4 = $request->file('img_option_d')->store('uploads');
        } else {
            $filename4 = $data->img_option_d;
        }

        if ($request->hasFile('img_option_e')) {
            $filename5 = $request->file('img_option_e')->store('uploads');
        } else {
            $filename5 = $data->img_option_e;
        }

        if ($request->hasFile('img_discussion')) {
            $filenamedisucussion = $request->file('img_discussion')->store('uploads');
        } else {
            $filenamedisucussion = $data->img_discussion;
        }

        $data->update([
            'jenis' => $request->jenis,
            'question' => $request->question,
            'option_a' => $request->option_a,
            'value_a' => $request->value_a,
            'option_b' => $request->option_b,
            'value_b' => $request->value_b,
            'option_c' => $request->option_c,
            'value_c' => $request->value_c,
            'option_d' => $request->option_d,
            'value_d' => $request->value_d,
            'option_e' => $request->option_e,
            'value_e' => $request->value_e,
            'answer' => $request->answer,
            'discussion' => $request->discussion,
            'img_question' => $file,
            'img_option_a' => $filename1,
            'img_option_b' => $filename2,
            'img_option_c' => $filename3,
            'img_option_d' => $filename4,
            'img_option_e' => $filename5,
            'img_discussion' => $filenamedisucussion,
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyImg(Request $request, $id)
    {
        $data = Question::findOrFail($id);

        if ($request->img_question) {
            $data->update([
                'img_question' => null,
            ]);
        }

        if ($request->img_option_a) {
            $data->update([
                'img_option_a' => null,
            ]);
        }

        if ($request->img_option_b) {
            $data->update([
                'img_option_b' => null,
            ]);
        }

        if ($request->img_option_c) {
            $data->update([
                'img_option_c' => null,
            ]);
        }

        if ($request->img_option_d) {
            $data->update([
                'img_option_d' => null,
            ]);
        }

        if ($request->img_option_e) {
            $data->update([
                'img_option_e' => null,
            ]);
        }

        Alert::success('Success!');

        return redirect()->back();
    }

    public function destroy($id)
    {
        $data = Question::findOrFail($id);

        $data->delete();

        Alert::success('Success!');

        return redirect()->back();
    }

    public function destroyAll($id)
    {
        $data = Package::findOrFail($id);

        $data->question()->delete();

        Alert::success('Success!');

        return redirect()->back();
    }

    public function export($id)
    {
        $package = Package::where('id', $id)->first()->name;

        return Excel::download(new QuestionsExport($id), "paket-soal-$package.xlsx");
    }
}
