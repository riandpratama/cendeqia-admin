<?php

namespace App\Http\Controllers;

use Storage;
use App\Models\Photo;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
    $photo1 = Photo::where('status', 1)->where('role', 1)->orderBy('id', 'asc')->get();
    $photo2 = Photo::where('status', 2)->where('role', 1)->orderBy('id', 'asc')->get();
    $photo3 = Photo::where('status', 3)->where('role', 1)->orderBy('id', 'asc')->get();

    $kedinasan1 = Photo::where('status', 1)->where('role', 2)->orderBy('id', 'asc')->get();
    $kedinasan2 = Photo::where('status', 2)->where('role', 2)->orderBy('id', 'asc')->get();
    $kedinasan3 = Photo::where('status', 3)->where('role', 2)->orderBy('id', 'asc')->get();

    return view('backend.dashboard.index', compact('photo1', 'photo2', 'photo3', 'kedinasan1', 'kedinasan2', 'kedinasan3'));
  }

  public function store(Request $request)
  {
    if ($request->hasFile('image')) {
      $photo = $request->file('image')->store('uploads/image');
    } else {
      $photo = '-';
    }

    Photo::create([
      'status' => $request->status,
      'image_path' => $photo,
      'role' => $request->role
    ]);

    return redirect()->back();
  }

  public function update(Request $request)
  {
    $photo = Photo::findOrFail($request->id);

    if ($request->hasFile('image')) {
      $file = $request->file('image')->store('uploads/image');
      if (asset('storages/' . $photo->image_path)) {
        Storage::delete($photo->image_path);
      }
    } else {
      $file = $photo->image_path;
    }

    $photo->update([
      'active' => $request->active,
      'image_path' => $file
    ]);

    return redirect()->back();
  }

  public function destroy($id)
  {
    $data = Photo::findOrFail($id);

    if (asset('storages/' . $data->image_path)) {
      Storage::delete($data->image_path);
    }

    $data->delete();

    return redirect()->back();
  }
}
