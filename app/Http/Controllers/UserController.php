<?php

namespace App\Http\Controllers;

use Alert;
use Session;

use Auth;
use App\User;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userAktif = User::orderBy('created_at', 'asc')->where('status', 'user')->where('isActive', 1)->get();
        $userTdkAktif = User::orderBy('created_at', 'asc')->where('status', 'user')->whereNotNull('email_verified_at')->where('isActive', 0)->get();
        $userRegis = User::orderBy('created_at', 'asc')->where('status', 'user')->where('isActive', 0)->where('email_verified_at')->get();
        $userUjiCoba = User::orderBy('created_at', 'asc')->where('status', 'simulation')->get();

        return view('backend.users.index', compact('userAktif', 'userTdkAktif', 'userRegis', 'userUjiCoba'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrfail($id);

        $user->update([
            'isActive' => 1
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateKedinasan($id)
    {
        $user = User::findOrfail($id);

        $user->update([
            'kedinasan' => 1
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrfail($id);

        return view('backend.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function nonActive($id)
    {
        $user = User::findOrfail($id);

        $user->update([
            'isActive' => 0
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }

    public function destroy($id)
    {
        $user = User::findOrfail($id);

        $user->delete();

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateEmail(Request $request)
    {
        $user = User::findOrfail($request->id);

        $user->update([
            'email' => $request->email
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }
}
