<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use App\Models\About;
use App\Models\Contact;
use Illuminate\Http\Request;

class FrontController extends Controller
{
    public function index()
    {
        $about = About::first();
        $contact = Contact::all();
        $faq = Faq::orderBy('id', 'asc')->get();

        return view('backend.front.index', compact('about', 'contact', 'faq'));
    }

    public function updateAbout(Request $request)
    {
        $data = About::findOrFail($request->id);

        $data->update([
            'description' => $request->description
        ]);

        return redirect()->back();
    }

    public function updateContact(Request $request)
    {
        $data = Contact::findOrFail($request->id);

        $data->update([
            'link' => $request->link,
            'description' => $request->description
        ]);

        return redirect()->back();
    }

    public function updateFaq(Request $request)
    {
        $data = Faq::findOrFail($request->id);

        $data->update([
            'title' => $request->title,
            'description' => $request->description
        ]);

        return redirect()->back();
    }
}
