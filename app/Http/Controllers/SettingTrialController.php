<?php

namespace App\Http\Controllers;

use DB;
use Alert;
use Session;
use App\Models\Suggestion;
use App\Imports\PackageImport;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SettingTrialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('simulations')->orderBy('created_at', 'asc')->get();

        return view('backend.setting-trial.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = DB::table('simulations')->where('id', $id)->update(['is_active' => $request->is_active]);

        return redirect()->back();
    }
}
