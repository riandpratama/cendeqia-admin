<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // User
        $data['userAktif'] = DB::table('users')->where('status', 'user')->where('isActive', 1)->get()->count();
        $data['userTdkAktif'] = DB::table('users')->where('status', 'user')->whereNotNull('email_verified_at')->where('isActive', 0)->get()->count();
        $data['userRegis'] = DB::table('users')->where('status', 'user')->where('isActive', 0)->where('email_verified_at')->get()->count();

        $data['kritiksaran'] = DB::table('suggestions')->get()->count();
        
        return view('home', $data);
    }
}
