<?php

namespace App\Http\Controllers;

use DB;
use Alert;
use Session;
use App\Models\Time;
use App\Models\DescriptionQuiz;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Time::first();
        $descriptionQuiz = DescriptionQuiz::first();

        return view('backend.time.index', compact('data', 'descriptionQuiz'));
    }

	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $time = Time::findOrFail($request->id);

        $time->update([
          'time' => $request->time,
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateDescriptionQuiz(Request $request)
    {
        $data = DescriptionQuiz::findOrFail($request->id);

        $data->update([
          'keterangan' => $request->keterangan,
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }
}