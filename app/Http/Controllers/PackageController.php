<?php

namespace App\Http\Controllers;

use Alert;
use Session;

use App\Models\Package;
use App\Models\Question;
use App\Imports\PackageImport;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Package::orderBy('urut', 'asc')->get();

        return view('backend.package.index', compact('data'));
    }

    public function storeExcel(Request $request)
    {
        if ($request->hasFile('excel')) {
            $file = $request->file('excel');
            Excel::import(new PackageImport, $file);
        }

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    
    }

    public function destroy($id)
    {
        
    }
}
