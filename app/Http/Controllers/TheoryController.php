<?php

namespace App\Http\Controllers;

use DB;
use Alert;
use Session;
use App\Models\Theory;
use App\Models\TheoryDetail;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TheoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Theory::orderBy('urut', 'asc')->get();

        return view('backend.theory.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $theory = Theory::findOrFail($request->id);

        if ($request->hasFile('image_path')) {
            $file = $request->file('image_path')->store('uploads/theory');
            if (asset('storages/' . $theory->image_path)) {
                Storage::delete($theory->image_path);
            }
        } else {
            $file = $theory->image_path;
        }

        $theory->update([
            'urut' => $request->urut,
            'name' => $request->name,
            'image_path' => $file,
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $theorydetail = TheoryDetail::where('theory_id', $request->id)->first();

        return view('backend.theory.show', compact('theorydetail'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateDetail(Request $request, $id)
    {
        $theory = TheoryDetail::findOrFail($request->id);

        if ($request->hasFile('file_download')) {
            $file = $request->file('file_download')->store('uploads/file-download');
        } else {
            $file = $theory->file_download;
        }

        $theory->update([
            'file_download' => $file,
            'file_url' => $request->file_url,
        ]);

        Alert::success('Success!');

        return redirect()->back();
    }
}
