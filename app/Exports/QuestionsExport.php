<?php

namespace App\Exports;

use App\Models\Question;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class QuestionsExport implements FromView
{
    public function __construct(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return \Illuminate\Support\FromView
     */
    public function view(): View
    {
        // return Question::query()->where('id', $this->id);
        // dd(Question::where('package_id', $this->id)->get());
        return view('exports.questions', [
            'questions' => Question::where('package_id', $this->id)->orderBy('urut', 'asc')->get()
        ]);
    }
}
