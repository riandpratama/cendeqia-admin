@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Teknisi</div>
                
                <div class="panel-body">
                    @if (Session::has('gagal'))
                        <div class="alert alert-danger alert-hidden" role="alert">
                            <strong>Gagal, Maaf ada yang salah</strong>
                        </div>
                    @endif
                    <a href="" class="btn btn-primary" style="margin-bottom: 15px;" data-title="Tambah" data-toggle="modal" data-target="#tambah">
                        <i class="fa fa-plus"></i>&nbsp;Tambah Data 
                    </a>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="datatables">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Teknisi</th>
                                    <th>Hapus </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                	<td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->nama_teknisi }}</td>
                                    <td>
                                        <form action="{{ route('teknisi.destroy', $item->id) }}" method="get">
                                            <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('Anda yakin ingin menyelesaikan?')">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md">
            <div class="modal-content">
              <div class="modal-header">
                <h4><i class="fa fa-plus"></i> Tambah data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>
                  <div class="modal-body">
                    <form action="{{ route('teknisi.store') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('nama_teknisi') ? ' has-error' : '' }}">
                            <label for="nama_teknisi">Nama Teknisi</label>
                            <input id="nama_teknisi" type="text" class="form-control" name="nama_teknisi" value="{{ old('nama_teknisi') }}">
                            @if ($errors->has('nama_teknisi'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nama_teknisi') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">Email</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password">Password</label>
                            <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-check"></span> Simpan</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
                        </div> 
                    </form>
                  </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

@endsection