@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><b>PASIVE 1:2</b></div>
                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="data1">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Teknisi </th>
                                    <th><center>Jumlah </center> </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data1 as $item)
                                <tr>
                                	<td>{{ $loop->iteration }}.</td>
                                    <td>{{ $item->nama_teknisi }}</td>
                                    <td><center>{{ $item->jumlah }}</center></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><b>PASIVE 1:4</b></div>
                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="data2">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Teknisi </th>
                                    <th><center>Jumlah </center></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data2 as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}.</td>
                                    <td>{{ $item->nama_teknisi }}</td>
                                    <td><center>{{ $item->jumlah }}</center></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><b>PASIVE 1:8</b></div>
                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="data3">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Teknisi </th>
                                    <th><center>Jumlah </center></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data3 as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}.</td>
                                    <td>{{ $item->nama_teknisi }}</td>
                                    <td><center>{{ $item->jumlah }}</center></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading"><b>PASIVE 1:16</b></div>
                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="data4">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Teknisi </th>
                                    <th><center>Jumlah </center></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data4 as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}.</td>
                                    <td>{{ $item->nama_teknisi }}</td>
                                    <td><center>{{ $item->jumlah }}</center></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
  	$(document).ready(function() {
	    $('#data1').dataTable();
	} );

    $(document).ready(function() {
        $('#data2').dataTable();
    } );

    $(document).ready(function() {
        $('#data3').dataTable();
    } );

    $(document).ready(function() {
        $('#data4').dataTable();
    } );

	function insertFormEdit(button){
        var item = $(button).data('item');
        console.log(item);
        console.log($('#formEdit .form-group #facebook_edit'));
        $('form#formEdit').attr('action','{{ url("admin/testimonial") }}/'+item.id+'/update');
        $('#formEdit .form-group #name').val(item.name);
        $('#formEdit .form-group #status').val(item.status);
        $('#formEdit .form-group #message').val(item.message);
    }
  </script>
@endsection