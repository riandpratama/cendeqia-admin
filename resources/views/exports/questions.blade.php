<table>
  <thead>
    <tr>
        <th>urut</th>
        <th>jenis</th>
        <th>question</th>
        <th>option_a</th>
        <th>value_a</th>
        <th>option_b</th>
        <th>value_b</th>
        <th>option_c</th>
        <th>value_c</th>
        <th>option_d</th>
        <th>value_d</th>
        <th>option_e</th>
        <th>value_e</th>
        <th>answer</th>
        <th>discussion</th>
    </tr>
  </thead>
  <tbody>
  @foreach($questions as $item)
      <tr>
          <td>{{ $item->urut }}</td>
          <td>{{ $item->jenis }}</td>
          <td>{{ $item->question }}</td>
          <td>{{ $item->option_a }}</td>
          <td>{{ $item->value_a }}</td>
          <td>{{ $item->option_b }}</td>
          <td>{{ $item->value_b }}</td>
          <td>{{ $item->option_c }}</td>
          <td>{{ $item->value_c }}</td>
          <td>{{ $item->option_d }}</td>
          <td>{{ $item->value_d }}</td>
          <td>{{ $item->option_e }}</td>
          <td>{{ $item->value_e }}</td>
          <td>{{ $item->answer }}</td>
          <td>{{ $item->discussion }}</td>
      </tr>
  @endforeach
  </tbody>
</table>