@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Data Waktu Pengerjaan
                </div>

                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Time</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                	<td>#</td>
                                    <td><b>{{ $data->time }}</b> (minutes)</td>
                                    <td>
                                        <button class="btn btn-xs btn-primary" onclick="ubah({{json_encode($data)}})" title="Ubah">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Keterangan Pengerjaan
                </div>

                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                	<td>#</td>
                                    <td>{!! $descriptionQuiz->keterangan !!}</td>
                                    <td>
                                        <button class="btn btn-xs btn-primary" onclick="ubah2({{json_encode($data)}})" title="Ubah">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-md" role="document">
           <div class="modal-content">
               <div class="modal-header">
                   <h4 class="modal-title" id="exampleModalLabel" style="font-weight: 600;"><span id="judul"></span></h4>
               </div>
               <form id="form" class="form-horizontal" method="post" enctype="multipart/form-data" >
               {{ csrf_field() }}
                  <input type="hidden" id="id" name="id">

                   <div class="modal-body" style="margin-left: 25px;">
                      <div class="row">
                           <div class="col-sm-3">Time (minutes)<span style="color: red;">*</span></div>
                           <div class="col-sm-8">
                                <input type="number" class="form-control" id="time" name="time" autocomplete="off" required>
                           </div>
                       </div> <br>
                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                       <button type="submit" class="btn btn-primary">Simpan</button>
                   </div>
               </form>
           </div>
       </div>
    </div>

    <div class="modal fade" id="modal2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-md" role="document">
           <div class="modal-content">
               <div class="modal-header">
                   <h4 class="modal-title" id="exampleModalLabel" style="font-weight: 600;"><span id="judul"></span></h4>
               </div>
               <form id="form2" class="form-horizontal" method="post" enctype="multipart/form-data" >
               {{ csrf_field() }}
                  <input type="hidden" id="id2" name="id">

                   <div class="modal-body" style="margin-left: 25px;">
                      <div class="row">
                           <div class="col-sm-3">Keterangan<span style="color: red;">*</span></div>
                           <div class="col-sm-8">
                                <textarea class="form-control" id="keterangan" name="keterangan">{{$descriptionQuiz->keterangan}}</textarea>
                                <!-- <input type="number" class="form-control" id="keterangan" name="keterangan" autocomplete="off" required> -->
                           </div>
                       </div> <br>
                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                       <button type="submit" class="btn btn-primary">Simpan</button>
                   </div>
               </form>
           </div>
       </div>
    </div>
@endsection

@section('js')
  <script type="text/javascript">
    function ubah(obj){
          $('#time').val(obj.time);
          $('#id').val(obj.id);

          $('#judul').html('Form Edit');
          $('#form').attr('action', "{{route('time.update')}}");
          $('#modal').modal('show');
    }

    function ubah2(obj){
          $('#keterangan').val(obj.keterangan);
          $('#id2').val(obj.id);

          $('#judul').html('Form Edit');
          $('#form2').attr('action', "{{route('description.quiz.update')}}");
          $('#modal2').modal('show');
    }
  </script>
    <script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
    <script>
        $('textarea').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script>
@endsection