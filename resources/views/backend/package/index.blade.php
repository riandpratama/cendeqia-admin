@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Data Paket</div>
                
                <div class="panel-body">
					{{-- <form action="{{ route('package.storeExcel') }}" style="width: 400px;" enctype="multipart/form-data" method="post">
                        @csrf
                        <input type="file" class="form-control" name="excel">
                        <i>File berbentuk excel</i>
                        <br>
                        <button class="btn btn-success btn-sm"> <i class="fa fa-file"></i> Import</button>               
                    </form> --}}
                    {{-- <hr> --}}
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="datatables">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Paket</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                	<td>{{ $loop->iteration }}.</td>
                                    <td>Paket {{ $item->name }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

@endsection