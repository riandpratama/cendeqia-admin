@extends('adminlte::page')

@section('css')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#1" data-toggle="tab"><b>CPNS</b></a>
                    </li>
                    <li>
                        <a href="#2" data-toggle="tab"><b>KEDINASAN</b></a>
                    </li>
                </ul>
                <div class="tab-content ">
                    <div class="tab-pane active" id="1">
                        @include('backend.dashboard.cpns')
                    </div>
                    <div class="tab-pane" id="2">
                        @include('backend.dashboard.kedinasan')
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('js')

  <script type="text/javascript">
    function tambah1() {
        $('#form1').attr('action', "{{route('dashboard.store')}}");
    }
    function ubah1(obj) {
      $('#image').val(obj.image);
      $('#active1').val(obj.active);
      $('#id').val(obj.id);
      $('#form1').attr('action', "{{route('dashboard.update')}}");
      $('#tambah1').modal('show');
    }

    function tambah2() {
        $('#form2').attr('action', "{{route('dashboard.store')}}");
    }
    function ubah2(obj) {
      $('#image').val(obj.image);
      $('#active2').val(obj.active);
      $('#id2').val(obj.id);
      $('#form2').attr('action', "{{route('dashboard.update')}}");
      $('#tambah2').modal('show');
    }

    function tambah3() {
        $('#form3').attr('action', "{{route('dashboard.store')}}");
    }
    function ubah3(obj) {
      $('#image').val(obj.image);
      $('#active3').val(obj.active);
      $('#id3').val(obj.id);
      $('#form3').attr('action', "{{route('dashboard.update')}}");
      $('#tambah3').modal('show');
    }
  </script>

  <!-- Kedinasan -->
  <script type="text/javascript">
    function tambah4() {
        $('#form4').attr('action', "{{route('dashboard.store')}}");
    }
    function ubah4(obj) {
      $('#image').val(obj.image);
      $('#active4').val(obj.active);
      $('#id4').val(obj.id);
      $('#form4').attr('action', "{{route('dashboard.update')}}");
      $('#tambah4').modal('show');
    }

    function tambah5() {
        $('#form5').attr('action', "{{route('dashboard.store')}}");
    }
    function ubah5(obj) {
      $('#image').val(obj.image);
      $('#active5').val(obj.active);
      $('#id5').val(obj.id);
      $('#form5').attr('action', "{{route('dashboard.update')}}");
      $('#tambah5').modal('show');
    }

    function tambah6() {
        $('#form6').attr('action', "{{route('dashboard.store')}}");
    }
    function ubah6(obj) {
      $('#image').val(obj.image);
      $('#active6').val(obj.active);
      $('#id6').val(obj.id);
      $('#form6').attr('action', "{{route('dashboard.update')}}");
      $('#tambah6').modal('show');
    }
  </script>

@endsection