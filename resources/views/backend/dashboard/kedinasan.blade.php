<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Banner 1</div>

            <div class="panel-body">
              <a href="" class="btn btn-primary" style="margin-bottom: 15px;" data-title="Tambah" data-toggle="modal" data-target="#tambah4" onclick="tambah4()">
                <i class="fa fa-plus"></i>&nbsp;Tambah
              </a>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover" id="datatables">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Image</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kedinasan1 as $item)
                            <tr>
                              <td>{{ $loop->iteration }}.</td>
                              <td><img src="{{ asset('storages/'.$item->image_path)}}" width="150px"></td>
                              <td>
                                <button class="btn btn-warning btn-sm" onclick="ubah4({{json_encode($item)}})"> <i class="fa fa-edit"></i> Update</button>
                                <form action="{{ route('dashboard.destroy', $item->id) }}" method="get" style="display: inline;">
                                    @csrf
                                    <button class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                                </form>
                                <span class="btn-badge btn-success"><b>{{$item->active}}</b></span>
                              </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
      <div class="panel panel-default">
          <div class="panel-heading">Banner 2</div>

          <div class="panel-body">
            <a href="" class="btn btn-primary" style="margin-bottom: 15px;" data-title="Tambah" data-toggle="modal" data-target="#tambah5" onclick="tambah5()">
              <i class="fa fa-plus"></i>&nbsp;Tambah
            </a>
              <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="datatables">
                      <thead>
                          <tr>
                              <th>No</th>
                              <th>Image</th>
                              <th>Aksi</th>
                          </tr>
                      </thead>
                      <tbody>
                          @foreach($kedinasan2 as $item)
                          <tr>
                            <td>{{ $loop->iteration }}.</td>
                            <td><img src="{{ asset('storages/'.$item->image_path)}}" width="150px"></td>
                            <td>
                                <button class="btn btn-warning btn-sm" onclick="ubah5({{json_encode($item)}})"> <i class="fa fa-edit"></i> Update</button>
                                <form action="{{ route('dashboard.destroy', $item->id) }}" method="get" style="display: inline;">
                                    @csrf
                                    <button class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                                </form>
                                <span class="btn-badge btn-success"><b>{{$item->active}}</b></span>
                            </td>
                          </tr>
                          @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>

  <div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-heading">Banner 3</div>

        <div class="panel-body">
          <a href="" class="btn btn-primary" style="margin-bottom: 15px;" data-title="Tambah" data-toggle="modal" data-target="#tambah6" onclick="tambah6()">
            <i class="fa fa-plus"></i>&nbsp;Tambah
          </a>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover" id="datatables">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Image</th>
                            <th>Hapus</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($kedinasan3 as $item)
                        <tr>
                          <td>{{ $loop->iteration }}.</td>
                          <td><img src="{{ asset('storages/'.$item->image_path)}}" width="150px"></td>
                          <td>
                            <button class="btn btn-warning btn-sm" onclick="ubah6({{json_encode($item)}})"> <i class="fa fa-edit"></i> Update</button>
                            <form action="{{ route('dashboard.destroy', $item->id) }}" method="get" style="display: inline;">
                                @csrf
                                <button class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                            </form>
                            <span class="btn-badge btn-success"><b>{{$item->active}}</b></span>
                          </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Modal 1 -->
<div class="modal fade" id="tambah4" tabindex="-1" role="dialog" aria-labelledby="tambah1" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          </div>
              <div class="modal-body">
                <form id="form4" action="{{ route('dashboard.store') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id4" />
                    <input type="hidden" name="status" value="1" />
                    <input type="hidden" name="role" value="2" />
                    <div class="form-group has-feedback {{ $errors->has('image') ? 'has-error' : '' }}">
                      <label>Upload</label>
                        <input type="file" name="image" class="form-control">
                        <span class="glyphicon glyphicon-book form-control-feedback"></span>
                        @if ($errors->has('image'))
                            <span class="help-block">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('image') ? 'has-error' : '' }}">
                      <label>Active (Image Awal)</label>
                        <select class="form-control" name="active" id="active4">
                          <option value="">Null</option>
                          <option value="active">active</option>
                        </select>
                        @if ($errors->has('image'))
                            <span class="help-block">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="modal-footer">
                  <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-check"></span> Simpan</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
                  </div>
                </form>
              </div>
          </div>
      </div>
  </div>

  <!-- Modal 2-->
  <div class="modal fade" id="tambah5" tabindex="-1" role="dialog" aria-labelledby="tambah1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
            <div class="modal-body">
              <form id="form5" action="{{ route('dashboard.store') }}" method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <input type="hidden" name="id" id="id5" />
                  <input type="hidden" name="status" value="2" />
                  <input type="hidden" name="role" value="2" />
                  <div class="form-group has-feedback {{ $errors->has('image') ? 'has-error' : '' }}">
                    <label>Upload</label>
                      <input type="file" name="image" class="form-control">
                      <span class="glyphicon glyphicon-book form-control-feedback"></span>
                      @if ($errors->has('image'))
                          <span class="help-block">
                              <strong>{{ $errors->first('image') }}</strong>
                          </span>
                      @endif
                  </div>
                  <div class="form-group has-feedback {{ $errors->has('image') ? 'has-error' : '' }}">
                    <label>Active (Image Awal)</label>
                      <select class="form-control" name="active" id="active5">
                        <option value="">Null</option>
                        <option value="active">active</option>
                      </select>
                      @if ($errors->has('image'))
                          <span class="help-block">
                              <strong>{{ $errors->first('image') }}</strong>
                          </span>
                      @endif
                  </div>
                  <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-check"></span> Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
                </div>
              </form>
            </div>
        </div>
    </div>
  </div>

  <!-- Modal 3-->
  <div class="modal fade" id="tambah6" tabindex="-1" role="dialog" aria-labelledby="tambah" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
          <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        </div>
            <div class="modal-body">
              <form id="form6" action="{{ route('dashboard.store') }}"  method="POST" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <input type="hidden" name="id" id="id6" />
                  <input type="hidden" name="status" value="3" />
                  <input type="hidden" name="role" value="2" />
                  <div class="form-group has-feedback {{ $errors->has('image') ? 'has-error' : '' }}">
                    <label>Upload</label>
                      <input type="file" name="image" class="form-control">
                      <span class="glyphicon glyphicon-book form-control-feedback"></span>
                      @if ($errors->has('image'))
                          <span class="help-block">
                              <strong>{{ $errors->first('image') }}</strong>
                          </span>
                      @endif
                  </div>
                  <div class="form-group has-feedback {{ $errors->has('image') ? 'has-error' : '' }}">
                    <label>Active (Image Awal)</label>
                      <select class="form-control" name="active" id="active6">
                        <option value="">Null</option>
                        <option value="active">active</option>
                      </select>
                      @if ($errors->has('image'))
                          <span class="help-block">
                              <strong>{{ $errors->first('image') }}</strong>
                          </span>
                      @endif
                  </div>
                  <div class="modal-footer">
                <button type="submit" class="btn btn-primary" onclick="return confirm('Anda yakin ingin menyelesaikan?')"><span class="glyphicon glyphicon-check"></span> Simpan</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Batal</button>
                </div>
              </form>
            </div>
        </div>
    </div>
  </div>