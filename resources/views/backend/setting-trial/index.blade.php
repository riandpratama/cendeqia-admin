@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Kritik & Saran</div>

                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="datatables">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}.</td>
                                    <td>{{ $item->status }}</td>
                                    <td>
                                      @if ($item->is_active == 1)
                                        <span class="badge" style="background-color: rgb(9, 9, 134);"> Aktif </span>
                                      @else
                                        <span class="badge" style="background-color: rgb(34, 134, 9);"> Tidak Aktif </span>
                                      @endif
                                    </td>
                                    <td>
                                      @if ($item->is_active == 0)
                                        <form action="{{ route('setting-trial.update', $item->id)}}" method="post">
                                          @csrf
                                          <input type="hidden" name="is_active" value="1">
                                          <button class="btn btn-primary btn-sm" onclick="return confirm('Anda yakin ingin mengaktifkan?')">Aktifkan</button>
                                        </form>
                                      @else
                                        <form action="{{ route('setting-trial.update', $item->id)}}" method="post">
                                          @csrf
                                          <input type="hidden" name="is_active" value="0">
                                          <button class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin menonaktifkan?')">Non Aktifkan</button>
                                        </form>
                                      @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

@endsection