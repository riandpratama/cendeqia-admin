@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
      <div class="col-md-7">
          <div class="panel panel-default">
              <div class="panel-heading">
              <b>File Preview</b>
              </div>

              <div class="panel-body">
                <a href="{{route('theory.index')}}" class="btn btn-danger btn-sm"> <i class="fa fa-arrow-left"></i> Kembali </a>
                  <div class="box-body table-responsive no-padding">
                  <hr>
                    {!!$theorydetail->file_url!!}
                  </div>
              </div>
          </div>
      </div>

      <div class="col-md-5">
          <div class="panel panel-default">
              <div class="panel-heading">
              <b>File Upload</b>
              </div>

              <div class="">
                    <div class="table-responsive no-padding">
                    <form id="form" class="form-horizontal" method="post" enctype="multipart/form-data" action="{{route('theorydetail.update', $theorydetail->id)}}" >
                    {{ csrf_field() }}
                        <input type="hidden" id="id" name="id" value="{{$theorydetail->id}}">
                        <div class="modal-body" style="margin-left: 25px;">
                            <div class="row">
                                <div class="col-sm-3">File Download<span style="color: red;">*</span></div>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" id="file_download" name="file_download">
                                </div>
                            </div> <br>
                            @if(!is_null($theorydetail->file_download))
                            <a href="{{$theorydetail->file_download}}" download>{{$theorydetail->file_download}}</a>
                            <hr>
                            @endif
                            <div class="row">
                                <div class="col-sm-3">File Preview<span style="color: red;">*</span></div>
                                <div class="col-sm-8">
                                    <textarea class="form-control" id="file_url" name="file_url"  rows="7" >@if(!is_null($theorydetail->file_url)){{$theorydetail->file_url}}@endif</textarea>
                                </div>
                            </div> <br>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary btn-sm">Update</button>
                        </div>
                    </form>
                    </div>
              </div>
          </div>
      </div>
  </div>
@endsection

@section('js')

@endsection