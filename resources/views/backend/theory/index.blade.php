@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
    @foreach($data as $item)
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                <button class="btn btn-xs btn-primary" onclick="ubah({{json_encode($item)}})" title="Ubah">
                    <i class="fa fa-edit"></i>
                </button> &nbsp;&nbsp;&nbsp;&nbsp;
                <b>{{ $item->name }}</b>
                </div>

                <div class="panel-body">
                    <a href="{{route('theory.show', $item->id)}}">
                        <div class="box-body table-responsive no-padding">
                          <img src="storages/{{$item->image_path}}" width="100%" height="100%">
                        </div>
                    </a>
                </div>
            </div>
        </div>
    @endforeach
  </div>

  <!-- Modal -->
  <div class="modal fade" id="modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-md" role="document">
           <div class="modal-content">
               <div class="modal-header">
                   <h4 class="modal-title" id="exampleModalLabel" style="font-weight: 600;"><span id="judul"></span></h4>
               </div>
               <form id="form" class="form-horizontal" method="post" enctype="multipart/form-data" >
               {{ csrf_field() }}
                  <input type="hidden" id="id" name="id">

                  <input type="hidden" id="group_user" name="group_user" value="{{Session::get('pilihan_gm')}}">
                   <div class="modal-body" style="margin-left: 25px;">
                      <div class="row">
                           <div class="col-sm-3">Keterangan Materi<span style="color: red;">*</span></div>
                           <div class="col-sm-8">
                                <input type="text" class="form-control" id="name" name="name" autocomplete="off" readonly="">
                           </div>
                       </div> <br>
                        <div class="row">
                            <div class="col-sm-3">Image <span style="color: red;">*</span></div>
                            <div class="col-sm-8">
                            <input type="file" class="form-control" name="image_path" />
                            </div>
                        </div><br>

                        <div class="row">
                           <div class="col-sm-3">Urutan <span style="color: red;">*</span></div>
                           <div class="col-sm-8">
                                <input type="text" class="form-control" id="urut" name="urut" autocomplete="off" required>
                           </div>
                       </div> <br>
                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                       <button type="submit" class="btn btn-primary">Simpan</button>
                   </div>
               </form>
           </div>
       </div>
    </div>

@endsection

@section('js')
  <script type="text/javascript">
    function ubah(obj){
          $('#name').val(obj.name);
          $('#urut').val(obj.urut);
          $('#id').val(obj.id);

          $('#judul').html('Form Edit');
          $('#form').attr('action', "{{route('theory.update')}}");
          $('#modal').modal('show');
    }
  </script>
@endsection