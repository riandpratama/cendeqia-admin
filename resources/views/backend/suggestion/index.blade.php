@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Kritik & Saran</div>
                
                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="datatables">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Member</th>
                                    <th>Email Member</th>
                                    <th>Tanggal</th>
                                    <th width="40">Kritik & Saran</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}.</td>
                                    <td>{{ $item->user->name }}</td>
                                    <td>{{ $item->user->email }}</td>
                                    <td>{{ date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td>
                                    <td>{!! $item->description !!}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

@endsection