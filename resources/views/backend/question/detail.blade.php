@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Soal Paket {{ $paket->name }}</div>

                <div class="panel-body">
					<form action="{{ route('question.storeExcel', $id) }}" style="width: 400px;" enctype="multipart/form-data" method="post" style="display: inline">
                        @csrf
                        <input type="file" class="form-control" name="excel">
                        <i style="color: red">*Harap input File dengan ekstensi excel</i>
                        <br>
                        <br>
                        <button class="btn btn-success btn-sm" onclick="return confirm('Anda yakin ingin mengimport data?')"> <i class="fa fa-file"></i> Import Excel</button>
                        <br>
                        <br>
                        <a href="{{route('question.export', $id)}}" class="btn btn-warning btn-sm" > <i class="fa fa-download"></i> Download Excel</a>
                    </form>
                    <br>
                    <form action="{{ route('question.destroyAll', $paket->id) }}">
                        <button class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin menghapus semua data?')">
                            <i class="fa fa-trash"></i> Delete All
                        </button>
                    </form>
                    <br>
                    <a href="{{ route('question.create', $paket->id) }}" class="btn btn-primary">
                        <i class="fa fa-plus" ></i> Tambah Soal
                    </a>
                    <hr>
                    <div class="box-body table-responsive no-padding">
                        <div class="table-responsive">
                        <table class="table table-hover" id="datatables">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Jenis Soal</th>
                                    <th>Nama Soal</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                	<td>{{ $item->urut }}.</td>
                                    <td>{{ $item->jenis }}</td>
                                    <td>{{ $item->question }}</td>
                                    <td style="width: 150px;">
                                        <a href="{{ route('question.edit', $item->id) }}" class="btn btn-primary btn-sm"> <i class="fa fa-edit"></i> Edit</a>
                                        <form action="{{ route('question.destroy', $item->id) }}" method="get" style="display: inline;">
                                            @csrf
                                            <button class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

@endsection