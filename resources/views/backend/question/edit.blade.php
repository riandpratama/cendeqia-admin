@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Paket: {{ $data->package->urut }} / Nomor Soal: {{ $data->urut }}</b>
                </div>
                
                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <form action="{{ route('question.update', $data->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('question') ? 'has-error' : '' }}">
                                    <label style="color:blue;">Soal/Pertanyaan</label>
                                    <textarea class="form-control" name="question" rows="3">{{ $data->question }}</textarea>
                                    @if ($errors->has('question'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('question') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('jenis') ? 'has-error' : '' }}">
                                    <label>Jenis Soal</label>
                                    <select name="jenis" id="jenis" class="form-control">
                                        <option value="TKP" @if($data->jenis == 'TKP') selected @endif>TKP</option>
                                        <option value="TIU" @if($data->jenis == 'TIU') selected @endif>TIU</option>
                                        <option value="TWK" @if($data->jenis == 'TWK') selected @endif>TWK</option>
                                    </select>
                                    @if ($errors->has('jenis'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('jenis') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('option_a') ? 'has-error' : '' }}">
                                    <label style="color:brown">Jawaban A</label>
                                    <input type="text" name="option_a" class="form-control" value="{{ $data->option_a }}">
                                    @if ($errors->has('option_a'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('option_a') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('option_b') ? 'has-error' : '' }}">
                                    <label style="color:purple;" >Jawaban B</label>
                                    <input type="text" name="option_b" class="form-control" value="{{ $data->option_b }}">
                                    @if ($errors->has('option_b'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('option_b') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('option_c') ? 'has-error' : '' }}">
                                    <label style="color:orange;" >Jawaban C</label>
                                    <input type="text" name="option_c" class="form-control" value="{{ $data->option_c }}">
                                    @if ($errors->has('option_c'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('option_c') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('option_d') ? 'has-error' : '' }}">
                                    <label style="color:red;" >Jawaban D</label>
                                    <input type="text" name="option_d" class="form-control" value="{{ $data->option_d }}">
                                    @if ($errors->has('option_d'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('option_d') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('option_e') ? 'has-error' : '' }}">
                                    <label style="color:pink;" >Jawaban E</label>
                                    <input type="text" name="option_e" class="form-control" value="{{ $data->option_e }}">
                                    @if ($errors->has('option_e'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('option_e') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('value_a') ? 'has-error' : '' }}">
                                        <label style="color:brown">Nilai A</label>
                                        <select name="value_a" id="" class="form-control">
                                            <option value="0" @if($data->value_a == '0') selected @endif>0</option>
                                            <option value="1" @if($data->value_a == '1') selected @endif>1</option>
                                            <option value="2" @if($data->value_a == '2') selected @endif>2</option>
                                            <option value="3" @if($data->value_a == '3') selected @endif>3</option>
                                            <option value="4" @if($data->value_a == '4') selected @endif>4</option>
                                            <option value="5" @if($data->value_a == '5') selected @endif>5</option>
                                        </select>
                                        @if ($errors->has('value_a'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('value_a') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('value_b') ? 'has-error' : '' }}">
                                        <label style="color:purple;" >Nilai B</label>
                                        <select name="value_b" id="" class="form-control">
                                            <option value="0" @if($data->value_b == '0') selected @endif>0</option>
                                            <option value="1" @if($data->value_b == '1') selected @endif>1</option>
                                            <option value="2" @if($data->value_b == '2') selected @endif>2</option>
                                            <option value="3" @if($data->value_b == '3') selected @endif>3</option>
                                            <option value="4" @if($data->value_b == '4') selected @endif>4</option>
                                            <option value="5" @if($data->value_b == '5') selected @endif>5</option>
                                        </select>
                                        @if ($errors->has('value_b'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('value_b') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('value_c') ? 'has-error' : '' }}">
                                        <label style="color:orange;" >Nilai C</label>
                                        <select name="value_c" id="" class="form-control">
                                            <option value="0" @if($data->value_c == '0') selected @endif>0</option>
                                            <option value="1" @if($data->value_c == '1') selected @endif>1</option>
                                            <option value="2" @if($data->value_c == '2') selected @endif>2</option>
                                            <option value="3" @if($data->value_c == '3') selected @endif>3</option>
                                            <option value="4" @if($data->value_c == '4') selected @endif>4</option>
                                            <option value="5" @if($data->value_c == '5') selected @endif>5</option>
                                        </select>
                                        @if ($errors->has('value_c'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('value_c') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('value_d') ? 'has-error' : '' }}">
                                        <label style="color:red;" >Nilai D</label>
                                        <select name="value_d" id="" class="form-control">
                                            <option value="0" @if($data->value_d == '0') selected @endif>0</option>
                                            <option value="1" @if($data->value_d == '1') selected @endif>1</option>
                                            <option value="2" @if($data->value_d == '2') selected @endif>2</option>
                                            <option value="3" @if($data->value_d == '3') selected @endif>3</option>
                                            <option value="4" @if($data->value_d == '4') selected @endif>4</option>
                                            <option value="5" @if($data->value_d == '5') selected @endif>5</option>
                                        </select>
                                        @if ($errors->has('value_d'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('value_d') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('value_e') ? 'has-error' : '' }}">
                                        <label style="color:pink;" >Nilai E</label>
                                        <select name="value_e" id="" class="form-control">
                                            <option value="0" @if($data->value_e == '0') selected @endif>0</option>
                                            <option value="1" @if($data->value_e == '1') selected @endif>1</option>
                                            <option value="2" @if($data->value_e == '2') selected @endif>2</option>
                                            <option value="3" @if($data->value_e == '3') selected @endif>3</option>
                                            <option value="4" @if($data->value_e == '4') selected @endif>4</option>
                                            <option value="5" @if($data->value_e == '5') selected @endif>5</option>
                                        </select>
                                        @if ($errors->has('value_e'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('value_e') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('answer') ? 'has-error' : '' }}">
                                        <label style="color: green;">Benar</label>
                                        <select name="answer" id="" class="form-control">
                                            <option value="a" @if($data->answer == 'a') selected @endif>A</option>
                                            <option value="b" @if($data->answer == 'b') selected @endif>B</option>
                                            <option value="c" @if($data->answer == 'c') selected @endif>C</option>
                                            <option value="d" @if($data->answer == 'd') selected @endif>D</option>
                                            <option value="e" @if($data->answer == 'e') selected @endif>E</option>
                                        </select>
                                        @if ($errors->has('answer'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('answer') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group has-feedback {{ $errors->has('img_question') ? 'has-error' : '' }}">
                                    <label style="color:blue;">Gambar Soal/Pertanyaan</label>
                                    <input type="file" name="img_question" class="form-control">
                                    @if ($errors->has('img_question'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_question') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('img_option_a') ? 'has-error' : '' }}">
                                    <label style="color:brown">Gambar Jawaban A</label>
                                    <input type="file" name="img_option_a" class="form-control">
                                    @if ($errors->has('img_option_a'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_option_a') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('img_option_b') ? 'has-error' : '' }}">
                                    <label style="color:purple;" >Gambar Jawaban B</label>
                                    <input type="file" name="img_option_b" class="form-control">
                                    @if ($errors->has('img_option_b'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_option_b') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('img_option_c') ? 'has-error' : '' }}">
                                    <label style="color:orange;" >Gambar Jawaban C</label>
                                    <input type="file" name="img_option_c" class="form-control">
                                    @if ($errors->has('img_option_c'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_option_c') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('img_option_d') ? 'has-error' : '' }}">
                                    <label style="color:red;" >Gambar Jawaban D</label>
                                    <input type="file" name="img_option_d" class="form-control">
                                    @if ($errors->has('img_option_d'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_option_d') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('img_option_e') ? 'has-error' : '' }}">
                                    <label style="color:pink;" >Gambar Jawaban E</label>
                                    <input type="file" name="img_option_e" class="form-control">
                                    @if ($errors->has('img_option_e'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_option_e') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('img_discussion') ? 'has-error' : '' }}">
                                    <label style="color:green;" >Gambar Pembahasan</label>
                                    <input type="file" name="img_discussion" class="form-control">
                                    @if ($errors->has('img_discussion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_discussion') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('discussion') ? 'has-error' : '' }}">
                                    <label style="color:green;">Pembahasan</label>
                                    <textarea class="form-control" name="discussion" rows="4">{{ $data->discussion }}</textarea>
                                    @if ($errors->has('discussion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('discussion') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <button type="submit"class="btn btn-primary btn-block btn-flat" onclick="return confirm('Anda yakin ingin menyelesaikan?')"> SIMPAN</button>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('question.detail', $data->package_id) }}" class="btn btn-warning btn-block btn-flat"> Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            @if (!is_null($data->img_question))
                <div class="col-md-2">
                <label for="">Gambar Soal/Pertanyaan</label>
                <img src="{{ asset('storages/'. $data->img_question) }}" alt="" width="150" height="150">
                <form action="{{ route('question.destroyImg', $data->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="img_question" value="img_question">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Anda yakin ingin menghapus?')" style="width: 150px;"><i class="fa fa-trash"></i></button>
                </form>
                </div>
            @endif

            @if (!is_null($data->img_option_a))
                <div class="col-md-2">
                <label for="">Gambar Jawaban A</label>
                <img src="{{ asset('storages/'. $data->img_option_a) }}" alt="" width="150" height="150">
                <form action="{{ route('question.destroyImg', $data->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="img_option_a" value="img_option_a">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Anda yakin ingin menghapus?')" style="width: 150px;"><i class="fa fa-trash"></i></button>
                </form>
                </div>
            @endif

            @if (!is_null($data->img_option_b))
                <div class="col-md-2">
                <label for="">Gambar Jawaban B</label>
                <img src="{{ asset('storages/'. $data->img_option_b) }}" alt="" width="150" height="150">
                <form action="{{ route('question.destroyImg', $data->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="img_option_b" value="img_option_b">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Anda yakin ingin menghapus?')" style="width: 150px;"><i class="fa fa-trash"></i></button>
                </form>
                </div>
            @endif

            @if (!is_null($data->img_option_c))
                <div class="col-md-2">
                <label for="">Gambar Jawaban C</label>
                <img src="{{ asset('storages/'. $data->img_option_c) }}" alt="" width="150" height="150">
                <form action="{{ route('question.destroyImg', $data->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="img_option_c" value="img_option_c">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Anda yakin ingin menghapus?')" style="width: 150px;"><i class="fa fa-trash"></i></button>
                </form>
                </div>
            @endif

            @if (!is_null($data->img_option_d))
                <div class="col-md-2">
                <label for="">Gambar Jawaban D</label>
                <img src="{{ asset('storages/'. $data->img_option_d) }}" alt="" width="150" height="150">
                <form action="{{ route('question.destroyImg', $data->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="img_option_d" value="img_option_d">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Anda yakin ingin menghapus?')" style="width: 150px;"><i class="fa fa-trash"></i></button>
                </form>
                </div>
            @endif

            @if (!is_null($data->img_option_e))
                <div class="col-md-2">
                <label for="">Gambar Jawaban E</label>
                <img src="{{ asset('storages/'. $data->img_option_e) }}" alt="" width="150" height="150">
                <form action="{{ route('question.destroyImg', $data->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="img_option_e" value="img_option_e">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Anda yakin ingin menghapus?')" style="width: 150px;"><i class="fa fa-trash"></i></button>
                </form>
                </div>
            @endif

            @if (!is_null($data->img_discussion))
                <div class="col-md-2">
                <label for="">Gambar Pembahasan</label>
                <img src="{{ asset('storages/'. $data->img_discussion) }}" alt="" width="150" height="150">
                <form action="{{ route('question.destroyImg', $data->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="img_discussion" value="img_discussion">
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Anda yakin ingin menghapus?')" style="width: 150px;"><i class="fa fa-trash"></i></button>
                </form>
                </div>
            @endif

        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

@endsection