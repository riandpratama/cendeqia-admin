@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Soal Berdasarkan Paket</div>
                
                <div class="panel-body">
					
                    <div class="box-body table-responsive no-padding">
                        <div class="table-responsive">
                        <table class="table table-hover" id="datatables">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Paket</th>
                                    <th>Jumlah Soal</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                	<td>{{ $loop->iteration }}</td>
                                    <td>Paket {{ $item->name }}</td>
                                    <td>{{ $item->question()->count() }}</td>
                                    <td><a href="{{ route('question.detail', $item->id) }}" class="btn btn-primary"> <i class="fa fa-eye"></i> Detail</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

@endsection