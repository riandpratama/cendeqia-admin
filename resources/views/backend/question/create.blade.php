@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>Paket: {{ $paket->urut }}</b>
                </div>
                
                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <form action="{{ route('question.store', $paket->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('question') ? 'has-error' : '' }}">
                                    <label style="color:black;">Nomor Soal</label>
                                    <select name="urut" id="urut" class="form-control">
                                        @for($i=1; $i<=100; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="form-group {{ $errors->has('question') ? 'has-error' : '' }}">
                                    <label style="color:blue;">Soal/Pertanyaan</label>
                                    <textarea class="form-control" name="question" rows="3"></textarea>
                                    @if ($errors->has('question'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('question') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('jenis') ? 'has-error' : '' }}">
                                    <label>Jenis Soal</label>
                                    <select name="jenis" id="jenis" class="form-control">
                                        <option value="TKP">TKP</option>
                                        <option value="TIU">TIU</option>
                                        <option value="TWK">TWK</option>
                                    </select>
                                    @if ($errors->has('jenis'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('jenis') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('option_a') ? 'has-error' : '' }}">
                                    <label style="color:brown">Jawaban A</label>
                                    <input type="text" name="option_a" class="form-control">
                                    @if ($errors->has('option_a'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('option_a') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('option_b') ? 'has-error' : '' }}">
                                    <label style="color:purple;" >Jawaban B</label>
                                    <input type="text" name="option_b" class="form-control">
                                    @if ($errors->has('option_b'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('option_b') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('option_c') ? 'has-error' : '' }}">
                                    <label style="color:orange;" >Jawaban C</label>
                                    <input type="text" name="option_c" class="form-control">
                                    @if ($errors->has('option_c'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('option_c') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('option_d') ? 'has-error' : '' }}">
                                    <label style="color:red;" >Jawaban D</label>
                                    <input type="text" name="option_d" class="form-control">
                                    @if ($errors->has('option_d'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('option_d') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('option_e') ? 'has-error' : '' }}">
                                    <label style="color:pink;" >Jawaban E</label>
                                    <input type="text" name="option_e" class="form-control">
                                    @if ($errors->has('option_e'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('option_e') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('value_a') ? 'has-error' : '' }}">
                                        <label style="color:brown">Nilai A</label>
                                        <select name="value_a" id="" class="form-control">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        @if ($errors->has('value_a'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('value_a') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('value_b') ? 'has-error' : '' }}">
                                        <label style="color:purple;" >Nilai B</label>
                                        <select name="value_b" id="" class="form-control">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        @if ($errors->has('value_b'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('value_b') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('value_c') ? 'has-error' : '' }}">
                                        <label style="color:orange;" >Nilai C</label>
                                        <select name="value_c" id="" class="form-control">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        @if ($errors->has('value_c'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('value_c') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('value_d') ? 'has-error' : '' }}">
                                        <label style="color:red;" >Nilai D</label>
                                        <select name="value_d" id="" class="form-control">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        @if ($errors->has('value_d'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('value_d') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('value_e') ? 'has-error' : '' }}">
                                        <label style="color:pink;" >Nilai E</label>
                                        <select name="value_e" id="" class="form-control">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        @if ($errors->has('value_e'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('value_e') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('answer') ? 'has-error' : '' }}">
                                        <label style="color: green;">Benar</label>
                                        <select name="answer" id="" class="form-control">
                                            <option value="a">A</option>
                                            <option value="b">B</option>
                                            <option value="c">C</option>
                                            <option value="d">D</option>
                                            <option value="e">E</option>
                                        </select>
                                        @if ($errors->has('answer'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('answer') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group has-feedback {{ $errors->has('img_question') ? 'has-error' : '' }}">
                                    <label style="color:blue;">Gambar Soal/Pertanyaan</label>
                                    <input type="file" name="img_question" class="form-control">
                                    @if ($errors->has('img_question'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_question') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('img_option_a') ? 'has-error' : '' }}">
                                    <label style="color:brown">Gambar Jawaban A</label>
                                    <input type="file" name="img_option_a" class="form-control">
                                    @if ($errors->has('img_option_a'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_option_a') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('img_option_b') ? 'has-error' : '' }}">
                                    <label style="color:purple;" >Gambar Jawaban B</label>
                                    <input type="file" name="img_option_b" class="form-control">
                                    @if ($errors->has('img_option_b'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_option_b') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('img_option_c') ? 'has-error' : '' }}">
                                    <label style="color:orange;" >Gambar Jawaban C</label>
                                    <input type="file" name="img_option_c" class="form-control">
                                    @if ($errors->has('img_option_c'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_option_c') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('img_option_d') ? 'has-error' : '' }}">
                                    <label style="color:red;" >Gambar Jawaban D</label>
                                    <input type="file" name="img_option_d" class="form-control">
                                    @if ($errors->has('img_option_d'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_option_d') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('img_option_e') ? 'has-error' : '' }}">
                                    <label style="color:pink;" >Gambar Jawaban E</label>
                                    <input type="file" name="img_option_e" class="form-control">
                                    @if ($errors->has('img_option_e'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_option_e') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group has-feedback {{ $errors->has('img_discussion') ? 'has-error' : '' }}">
                                    <label style="color:green;" >Gambar Pembahasan</label>
                                    <input type="file" name="img_discussion" class="form-control">
                                    @if ($errors->has('img_discussion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('img_discussion') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('discussion') ? 'has-error' : '' }}">
                                    <label style="color:green;">Pembahasan</label>
                                    <textarea class="form-control" name="discussion" rows="4"></textarea>
                                    @if ($errors->has('discussion'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('discussion') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <button type="submit"class="btn btn-primary btn-block btn-flat" onclick="return confirm('Anda yakin ingin menyelesaikan?')"> SIMPAN</button>
                                </div>
                                <div class="col-md-6">
                                    <a href="{{ route('question.detail', $paket->id) }}" class="btn btn-warning btn-block btn-flat"> Kembali</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables').DataTable();
    });
</script>

@endsection