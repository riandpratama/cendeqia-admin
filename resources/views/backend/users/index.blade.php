@extends('adminlte::page')

@section('css')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Data Seluruh User</div>

                <div>
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a  href="#1" data-toggle="tab">Member Menunggu Verifikasi</a>
                        </li>
                        <li>
                            <a href="#2" data-toggle="tab">Member Aktif</a>
                        </li>
                        <li>
                            <a href="#3" data-toggle="tab">Member Tidak Verifikasi Email</a>
                        </li>
                        <li>
                            <a href="#4" data-toggle="tab">Member Uji Coba</a>
                        </li>
                    </ul>
                    <div class="tab-content ">
                        <div class="tab-pane active" id="1">
                            <div class="panel-body">
                                <div class="table-responsive">
                                <table class="table table-hover" id="datatables1">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>Member Verifikasi Email</th>
                                            <th>Aksi</th>
                                            <th>Verifikasi CPNS</th>
                                            <th>Verifikasi Kedinasan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($userTdkAktif as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ date('d/m/Y H:i:s', strtotime($item->email_verified_at)) }}</td>
                                            <td>
                                                <a href="{{ route('user.show', $item->id) }}" class="btn btn-success btn-sm"> <i class="fa fa-eye"></i> Detail</a>
                                            </td>
                                            <td>
                                                @if ($item->cpns == 1)
                                                <form action="{{ route('user.update', $item->id) }}" method="POST" style="display: inline;">
                                                    @csrf
                                                    <button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Anda yakin ingin verifikasi?')">
                                                        <i class="fa fa-check"></i> &nbsp; Verifikasi CPNS
                                                    </button>
                                                </form>
                                                @else
                                                -
                                                @endif
                                            </td>
                                            <td>
                                                @if ($item->kedinasan == 1)
                                                <form action="{{ route('user.update', $item->id) }}" method="POST" style="display: inline;">
                                                    @csrf
                                                    <button type="submit" class="btn btn-warning btn-sm" onclick="return confirm('Anda yakin ingin verifikasi?')">
                                                        <i class="fa fa-check"></i> &nbsp; Verifikasi Kedinasan
                                                    </button>
                                                </form>
                                                @else
                                                -
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="2">
                            <div class="panel-body">
                                <div class="table-responsive">
                                <table class="table table-hover" id="datatables2">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>Member Verifikasi Email</th>
                                            <th>Status</th>
                                            <th>Verifikasi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($userAktif as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ date('d/m/Y H:i:s', strtotime($item->email_verified_at)) }}</td>
                                            <td>
                                                @if ($item->cpns == 1)
                                                    <span class="badge" style="background-color:#3c8dbc;">CPNS</span>
                                                @endif
                                                @if ($item->kedinasan == 1)
                                                    <span class="badge bg-warning" style="background-color: #f39c12">Kedinasan</span>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($item->cpns == 0)
                                                <form action="{{ route('user.update', $item->id) }}" method="POST" style="display: inline;">
                                                    @csrf
                                                    <button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Anda yakin ingin verifikasi?')">
                                                        <i class="fa fa-plus"></i> &nbsp; Tambahkan Akses CPNS
                                                    </button>
                                                </form>
                                                @endif
                                                @if ($item->kedinasan == 0)
                                                <form action="{{ route('user.update.kedinasan', $item->id) }}" method="POST" style="display: inline;">
                                                    @csrf
                                                    <button type="submit" class="btn btn-warning btn-sm" onclick="return confirm('Anda yakin ingin verifikasi?')">
                                                        <i class="fa fa-plus"></i> &nbsp; Tambahkan Akses Kedinasan
                                                    </button>
                                                </form>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('user.show', $item->id) }}" class="btn btn-success btn-sm"> <i class="fa fa-eye"></i> Detail</a>
                                                <form action="{{ route('user.nonActive', $item->id) }}" method="POST" style="display: inline;">
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Anda yakin ingin non aktifkan user?')">
                                                        <i class="fa fa-unlock-alt"></i> &nbsp; Non Aktif
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="3">
                            <div class="panel-body">
                                <div class="table-responsive">
                                <table class="table table-hover" id="datatables3">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>Tanggal Registrasi</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($userRegis as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td>
                                            <td>
                                                <a href="{{ route('user.show', $item->id) }}" class="btn btn-success btn-sm"> <i class="fa fa-eye"></i> Detail</a>
                                                <form action="{{ route('user.destroy', $item->id) }}" style="display: inline;">
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Hapus</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="4">
                            <div class="panel-body">
                                <div class="table-responsive">
                                <table class="table table-hover" id="datatables4">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Email</th>
                                            <th>No. Handphone</th>
                                            <th>Status</th>
                                            <th>Tanggal</th>
                                            <th>Update</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($userUjiCoba as $item)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->phone }}</td>
                                            <td>
                                                @if ($item->cpns == 1)
                                                    <span class="badge" style="background-color:#3c8dbc;">CPNS</span>
                                                @endif
                                                @if ($item->kedinasan == 1)
                                                    <span class="badge bg-warning" style="background-color: #f39c12">Kedinasan</span>
                                                @endif
                                            </td>
                                            <td>{{ date('d/m/Y H:i:s', strtotime($item->created_at)) }}</td>
                                            <td>
                                                <button class="btn btn-xs btn-success" onclick="ubah({{json_encode($item)}})" title="Ubah">
                                                    <i class="fa fa-edit"></i>
                                                </button> &nbsp;&nbsp;&nbsp;&nbsp;
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
  <div class="modal fade" id="modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel" style="font-weight: 600;"><span id="judul"></span></h4>
            </div>
            <form id="form" class="form-horizontal" method="post" enctype="multipart/form-data" >
            {{ csrf_field() }}
               <input type="hidden" id="id" name="id">
                <div class="modal-body" style="margin-left: 25px;">
                   <div class="row">
                        <div class="col-sm-3">Ubah Email<span style="color: red;">*</span></div>
                        <div class="col-sm-8">
                             <input type="text" class="form-control" id="email" name="email" autocomplete="off">
                        </div>
                    </div> <br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
 </div>

@endsection

@section('js')
<script type="text/javascript">
    $(function () {
        $('#datatables1').DataTable();
    });
    $(function () {
        $('#datatables2').DataTable();
    });
    $(function () {
        $('#datatables3').DataTable();
    });
    $(function () {
        $('#datatables4').DataTable();
    });
</script>

<script type="text/javascript">
    function ubah(obj){
          $('#email').val(obj.email);
          $('#id').val(obj.id);

          $('#judul').html('Form Edit');
          $('#form').attr('action', "{{route('user.updateEmail')}}");
          $('#modal').modal('show');
    }
</script>

@endsection