@extends('adminlte::page')

@section('css')

@section('content')
	<div class="row">
		<div class="col-md-12">
		<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Detail member</h3>
            </div>
            <form class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-8">
                    <input type="email" class="form-control" id="inputEmail3" value="{{ $user->email }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputPassword3" value="{{ $user->name }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Alamat</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputPassword3" value="{{ $user->address }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Kota</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputPassword3" value="{{ $user->city }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Province</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputPassword3" value="{{ $user->province }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jenis Kelamin</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputPassword3" value="{{ $user->gender }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Social media (instagram)</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputPassword3" value="{{ $user->social_media }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Nomor Handphone</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="inputPassword3" value="{{ $user->phone }}" readonly="">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Foto</label>
                  <div class="col-sm-8">
                    <img src="https://cendeqia.org/storages/{{ $user->photo }}" alt="">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="{{ route('user.index') }}" class="btn btn-default">Kembali</a>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
	</div>
@endsection