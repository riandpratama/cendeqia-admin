@extends('adminlte::page')

@section('css')

@section('content')

	<div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tentang
                </div>

                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Judul</th>
                                  <th>Deskripsi</th>
                                  <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                	<td>#</td>
                                  <td><b></b> {{$about->title}} </td>
                                  <td><b></b> {!!$about->description!!} </td>
                                  <td>
                                      <button class="btn btn-xs btn-primary" onclick="ubahAbout({{json_encode($about)}})" title="Ubah">
                                          <i class="fa fa-edit"></i>
                                      </button>
                                  </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Kontak
                </div>

                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>Keterangan</th>
                                    <th>Link</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contact as $item)
                                <tr>
                                	<td>#</td>
                                    <td>{{$item->title}}</td>
                                    <td>{!!$item->description!!}</td>
                                    <td>{{$item->link}}</td>
                                    <td>
                                        <button class="btn btn-xs btn-primary" onclick="ubahContact({{json_encode($item)}})" title="Ubah">
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    FAQ
                </div>

                <div class="panel-body">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Judul</th>
                                  <th>Deskripsi</th>
                                  <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($faq as $item)
                                <tr>
                                  <td>{{$loop->iteration}}</td>
                                  <td><b></b> {{$item->title}} </td>
                                  <td><b></b> {!!$item->description!!} </td>
                                  <td>
                                      <button class="btn btn-xs btn-primary" onclick="ubahFaq({{json_encode($item)}})" title="Ubah">
                                          <i class="fa fa-edit"></i>
                                      </button>
                                  </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg" role="document">
           <div class="modal-content">
               <div class="modal-header">
                   <h4 class="modal-title" id="exampleModalLabel" style="font-weight: 600;"><span id="judul"></span></h4>
               </div>
               <form id="form" class="form-horizontal" method="post" enctype="multipart/form-data" >
               {{ csrf_field() }}
                  <input type="hidden" id="id" name="id">

                   <div class="modal-body">
                      <div class="row">
                           <div class="col-sm-12">
                                <textarea class="form-control" id="description" name="description"></textarea>
                           </div>
                       </div> <br>
                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                       <button type="submit" class="btn btn-primary">Simpan</button>
                   </div>
               </form>
           </div>
       </div>
    </div>

    <div class="modal fade" id="modal2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-md" role="document">
           <div class="modal-content">
               <div class="modal-header">
                   <h4 class="modal-title" id="exampleModalLabel" style="font-weight: 600;"><span id="judul"></span></h4>
               </div>
               <form id="form2" class="form-horizontal" method="post" enctype="multipart/form-data" >
               {{ csrf_field() }}
                  <input type="hidden" id="id2" name="id">

                   <div class="modal-body" style="margin-left: 25px;">
                    <div class="row">
                        <div class="col-sm-3">Link<span style="color: red;">*</span></div>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="link" name="link" autocomplete="off" required>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                           <div class="col-sm-3">Keterangan<span style="color: red;">*</span></div>
                           <div class="col-sm-8">
                                <textarea class="form-control" id="description2" name="description"></textarea>
                                <!-- <input type="number" class="form-control" id="keterangan" name="keterangan" autocomplete="off" required> -->
                           </div>
                       </div> <br>
                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                       <button type="submit" class="btn btn-primary">Simpan</button>
                   </div>
               </form>
           </div>
       </div>
    </div>

    <div class="modal fade" id="modal3" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel" style="font-weight: 600;"><span id="judul"></span></h4>
                </div>
                <form id="form3" class="form-horizontal" method="post" enctype="multipart/form-data" >
                {{ csrf_field() }}
                   <input type="hidden" id="id3" name="id">

                    <div class="modal-body" style="margin-left: 25px;">
                     <div class="row">
                         <div class="col-sm-3">Judul<span style="color: red;">*</span></div>
                         <div class="col-sm-8">
                             <input type="text" class="form-control" id="title" name="title" autocomplete="off" required>
                         </div>
                     </div>
                     <br>
                     <div class="row">
                            <div class="col-sm-3">Keterangan<span style="color: red;">*</span></div>
                            <div class="col-sm-8">
                                 <textarea class="form-control" id="description3" name="description"></textarea>
                            </div>
                        </div> <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
     </div>
@endsection

@section('js')
  <script type="text/javascript">
    function ubahAbout(obj){
          $('#title').val(obj.title);
          $('#description').val(obj.description);
          $('#id').val(obj.id);

          $('#judul').html('Form Edit');
          $('#form').attr('action', "{{route('front.about.update')}}");
          $('#modal').modal('show');
    }

    function ubahContact(obj){
          $('#link').val(obj.link);
          $('#description2').val(obj.description);
          $('#id2').val(obj.id);

          $('#judul').html('Form Edit');
          $('#form2').attr('action', "{{route('front.contact.update')}}");
          $('#modal2').modal('show');
    }

    function ubahFaq(obj){
          $('#title').val(obj.title);
          $('#description3').val(obj.description);
          $('#id3').val(obj.id);

          $('#judul').html('Form Edit');
          $('#form3').attr('action', "{{route('front.faq.update')}}");
          $('#modal3').modal('show');
    }
  </script>
    <script src="{{asset('/vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
    <script>
        $('textarea').ckeditor();
        // $('.textarea').ckeditor(); // if class is prefered.
    </script>
@endsection