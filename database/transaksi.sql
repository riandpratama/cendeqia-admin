-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 24 Nov 2019 pada 21.45
-- Versi server: 10.2.27-MariaDB-cll-lve
-- Versi PHP: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `alphacre_database`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tanggal` date NOT NULL,
  `transaksi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `debet` int(11) DEFAULT NULL,
  `kredit` int(11) DEFAULT NULL,
  `saldo` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id`, `tanggal`, `transaksi`, `debet`, `kredit`, `saldo`, `created_at`, `updated_at`) VALUES
(7, '2019-08-10', 'Saldo Awal', 300000, NULL, 300000, '2019-09-25 07:21:12', '2019-09-25 07:21:12'),
(8, '2019-08-10', 'Pembelian Domain dan Hosting', NULL, 260000, 40000, '2019-09-25 07:21:57', '2019-09-25 07:21:57'),
(12, '2019-09-25', 'Pembelian Biaya Pulsa', NULL, 22000, 18000, '2019-09-25 09:46:53', '2019-09-25 09:46:53'),
(13, '2019-09-27', 'Design logo', 100000, NULL, 118000, '2019-10-14 00:16:50', '2019-10-14 00:16:50'),
(14, '2019-10-14', 'Design brosur', 60000, NULL, 178000, '2019-10-14 00:18:19', '2019-10-14 00:18:19'),
(15, '2019-10-17', 'Design banner', 50000, NULL, 228000, '2019-10-16 22:14:36', '2019-10-16 22:14:36'),
(16, '2019-10-19', 'DP web mas dio', 300000, NULL, 528000, '2019-10-19 09:57:33', '2019-10-19 09:57:33'),
(17, '2019-10-30', 'DP web profil tanggulrejo', 500000, NULL, 1028000, '2019-10-30 09:58:01', '2019-10-30 09:58:01'),
(18, '2019-11-04', 'Desain brosur + banner', 100000, NULL, 1128000, '2019-11-03 23:36:07', '2019-11-03 23:36:07'),
(19, '2019-11-05', 'DP web mas dio 2', 200000, NULL, 1328000, '2019-11-05 04:53:55', '2019-11-05 04:53:55'),
(20, '2019-11-08', 'Design x banner ache', 50000, NULL, 1378000, '2019-11-08 05:33:12', '2019-11-08 05:33:12'),
(21, '2019-11-08', 'Netralisasi pulsa', 22000, NULL, 1400000, '2019-11-08 05:34:23', '2019-11-08 05:34:23'),
(25, '2019-11-08', 'paid promote mipa unesa', NULL, 35000, 1365000, '2019-11-16 10:21:26', '2019-11-16 10:21:26');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
