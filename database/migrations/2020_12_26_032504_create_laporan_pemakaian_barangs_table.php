<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporanPemakaianBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_pemakaian_barang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal');
            $table->integer('master_teknisi_id');
            $table->integer('master_barang_id');
            $table->string('nomor_tiket');
            $table->string('nomor_label_odp');
            $table->text('file_foto_label_odp');
            $table->text('file_foto_sebelum_dipasang');
            $table->text('file_foto_sesudah_dipasang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_pemakaian_barang');
    }
}
