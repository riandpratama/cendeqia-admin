<?php

Route::get('/cache', function () {
	Artisan::call('config:cache');
});

// Auth::routes();

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@postLogin')->name('login.post');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {

	Route::get('dashboard', 'DashboardController@index');
	Route::post('dashboard/store', 'DashboardController@store')->name('dashboard.store');
	Route::post('dashboard/update', 'DashboardController@update')->name('dashboard.update');
	Route::get('dashboard/destroy/{id}', 'DashboardController@destroy')->name('dashboard.destroy');

	Route::get('package', 'PackageController@index');
	Route::post('package', 'PackageController@storeExcel')->name('package.storeExcel');

	Route::get('questions', 'QuestionController@index');
	Route::get('question/{id}', 'QuestionController@detail')->name('question.detail');
	Route::post('question/{id}', 'QuestionController@storeExcel')->name('question.storeExcel');
	Route::get('question-create/{id}', 'QuestionController@create')->name('question.create');
	Route::post('question-store/{id}', 'QuestionController@store')->name('question.store');
	Route::get('question-edit/{id}', 'QuestionController@edit')->name('question.edit');
	Route::post('question-update/{id}', 'QuestionController@update')->name('question.update');
	Route::post('destroy-img/{id}', 'QuestionController@destroyImg')->name('question.destroyImg');
	Route::get('destroy/{id}', 'QuestionController@destroy')->name('question.destroy');
	Route::get('destroy-all/{id}', 'QuestionController@destroyAll')->name('question.destroyAll');
	Route::get('question/export/{id}', 'QuestionController@export')->name('question.export');

	Route::get('users', 'UserController@index')->name('user.index');
	Route::get('users/{id}', 'UserController@show')->name('user.show');
	Route::post('users/{id}/update', 'UserController@update')->name('user.update');
	Route::get('users/{id}/delete', 'UserController@destroy')->name('user.destroy');
	Route::post('users/{id}/non-active', 'UserController@nonActive')->name('user.nonActive');
	Route::post('users/update/email', 'UserController@updateEmail')->name('user.updateEmail');

	Route::post('users/{id}/update/kedinasan', 'UserController@updateKedinasan')->name('user.update.kedinasan');

	Route::get('suggestions', 'KritikSaranController@index')->name('suggestion.index');

	Route::get('setting-trial', 'SettingTrialController@index')->name('setting-trial.index');
	Route::post('setting-trial/{id}', 'SettingTrialController@update')->name('setting-trial.update');

	Route::get('theory', 'TheoryController@index')->name('theory.index');
	Route::post('theory-update', 'TheoryController@update')->name('theory.update');
	Route::get('theory/{id}', 'TheoryController@show')->name('theory.show');
	Route::post('theorydetailupdate/{id}', 'TheoryController@updateDetail')->name('theorydetail.update');

	Route::get('time', 'TimeController@index')->name('time.index');
	Route::post('time-update', 'TimeController@update')->name('time.update');
	Route::post('description-quiz-update', 'TimeController@updateDescriptionQuiz')->name('description.quiz.update');

	Route::get('setting-front', 'FrontController@index')->name('front.index');
	Route::post('setting-about-update', 'FrontController@updateAbout')->name('front.about.update');
	Route::post('setting-contact-update', 'FrontController@updateContact')->name('front.contact.update');
	Route::post('setting-faq-update', 'FrontController@updateFaq')->name('front.faq.update');
	// Route::post('description-quiz-update', 'FrontController@updateDescriptionQuiz')->name('description.quiz.update');

	// Route::post('teknisi/store', 'TeknisiController@store')->name('teknisi.store');
	// Route::get('teknisi/destroy/{id}', 'TeknisiController@destroy')->name('teknisi.destroy');
});
